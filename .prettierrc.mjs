/** @type {import("prettier").Config} */
const config = {
	printWidth: 80,
	semi: true,
	singleQuote: false,
	tabWidth: 4,
	trailingComma: "es5",
	useTabs: false,
	arrowParens: "always",
	experimentalTernaries: true,
};

export default config;
