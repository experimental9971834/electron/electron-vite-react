import { useState } from "react";

import reactLogo from "@/assets/react.svg";
import { Button } from "@/components/ui/button";
import viteLogo from "/electron-vite.animate.svg";

function App() {
	const [count, setCount] = useState(0);

	return (
		<div className="min-h-dvh bg-gradient-to-b from-blue-600 to-indigo-600 bg-no-repeat bg-cover grid place-content-center gap-4 text-white">
			<div className="flex gap-4 items-center">
				<a href="https://electron-vite.github.io" target="_blank">
					<img src={viteLogo} className="size-40" alt="Vite logo" />
				</a>
				<a href="https://react.dev" target="_blank">
					<img
						src={reactLogo}
						className="size-40 animate-pulse"
						alt="React logo"
					/>
				</a>
			</div>
			<h1 className="text-center text-3xl font-semibold">Vite + React</h1>
			<div className="text-center space-y-2">
				<p>
					Count is <span className="font-mono">{count}</span>
				</p>
				<div className="flex gap-4 justify-center">
					<Button
						onClick={() => setCount((prev) => prev + 1)}
						className="bg-white rounded-full text-black font-semibold hover:bg-gray-200"
					>
						Increment
					</Button>
					<Button
						onClick={() =>
							setCount((prev) => (prev < 1 ? 0 : prev - 1))
						}
						className="bg-white rounded-full text-black font-semibold hover:bg-gray-200"
					>
						Decrement
					</Button>
				</div>
				<p>
					Edit
					<code className="font-mono bg-white bg-opacity-15 px-2 mx-1 rounded">
						src/App.tsx
					</code>
					and save to test HMR
				</p>
			</div>
			<p className="read-the-docs">
				Click on the Vite and React logos to learn more
			</p>
		</div>
	);
}

export default App;
